""" Блок сравнения документов """
__author__ = 'ma.razgovorov'

from blockly_executor.core.helpers.compare_objects import compare_objects
from blockly_executor.core.block_templates.simple_block import SimpleBlock


class CompareObjects(SimpleBlock):
    """
    Блок сравнения документов. Если объекты различны, будет возвращена структура расхождения.
    На входе принимает параметры, указаные в описании метода _calc_value.
    Возвращает расхождение по объекту в рамках общей структуры объекта, если они имеются, иначе None.
    В итоговом объекте будут все реквизиты из приоритетного объекта (doc_src) в исходном виде, кроме сравниваемых.
    Если при сравнении структуры, данные приоритетного объекта будут отсутствовать (например строка ТЧ),
    структура будет взята из второго объекта.
    На блоке указываются параметры:
        OBJECT1  - Объекты, которые сравниваются. Присоединяемые блоки. API3 объект.
        OBJECT2

        PROPS - Массив реквизитов шапки объекта для сравнения. Присоединяемый блок. Массив.

        Настраиваемые присоединяемые блоки
        TABLE{index}_NAME - Имя подобъекта для сравнения. Указывается на блоке. Строка.
        TABLE{index}_GROUP - Список ключей для сопоставления строк подобъекта. Присоединяемый блок. Массив.
        TABLE{index}_FIELDS - Список полей для сравнения подобъектов. Присоединяемый блок. Массив.
    """

    async def _calc_value(self, node, path, context, block_context):
        """
        Расчет значения блока.
        Расчёт различий полученных объектов в соответствии с логикой в описании класса.
        :param node:
        :param path:
        :param context:
        :param block_context: Контекст выполнения блока с указанными и рассчитанными значениями.
        :return: Объекты с расхождениями, если они имеются. Иначе None.
        :rtype: dict
        """
        return compare_objects(block_context.get('OBJECT1'), block_context.get('OBJECT2'), block_context)
